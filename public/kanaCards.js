// if (window.navigator.userAgent.split('; ')[1].indexOf('Android') !== -1){Android}

//
// DONNÉES
//

const consonants = ['ø'/**/, 'k', 's', 't', 'n', 'h', 'm', 'y', 'r', 'w', 'g', 'z', 'd', 'b', 'p'/**/]

const hiragana = {
    ø: [['あ', 'a'], ['い', 'i'], ['う', 'u'], ['え', 'e'], ['お', 'o']],
    k: [['か', 'ka'], ['き', 'ki'], ['く', 'ku'], ['け', 'ke'], ['こ', 'ko']],
    s: [['さ', 'sa'], ['し', 'shi'], ['す', 'su'], ['せ', 'se'], ['そ', 'so']],
    t: [['た', 'ta'], ['ち', 'chi'], ['つ', 'tsu'], ['て', 'te'], ['と', 'to']],
    n: [['な', 'na'], ['に', 'ni'], ['ぬ', 'nu'], ['ね', 'ne'], ['の', 'no']],
    h: [['は', 'ha'], ['ひ', 'hi'], ['ふ', 'fu'], ['へ', 'he'], ['ほ', 'ho']],
    m: [['ま', 'ma'], ['み', 'mi'], ['む', 'mu'], ['め', 'me'], ['も', 'mo']],
    y: [['や', 'ya'], ['', ''], ['ゆ', 'yu'], ['', ''], ['よ', 'yo']],
    r: [['ら', 'ra'], ['り', 'ri'], ['る', 'ru'], ['れ', 're'], ['ろ', 'ro']],
    w: [['わ', 'wa'], ['', ''], ['ん', 'n'], ['', ''], ['を', 'wo']],
    g: [['が', 'ga'], ['ぎ', 'gi'], ['ぐ', 'gu'], ['げ', 'ge'], ['ご', 'go']],
    z: [['ざ', 'za'], ['じ', 'ji'], ['ず', 'zu'], ['ぜ', 'ze'], ['ぞ', 'zo']],
    d: [['だ', 'da'], ['ぢ', 'di'], ['づ', 'du'], ['で', 'de'], ['ど', 'do']],
    b: [['ば', 'ba'], ['び', 'bi'], ['ぶ', 'bu'], ['べ', 'be'], ['ぼ', 'bo']],
    p: [['ぱ', 'pa'], ['ぴ', 'pi'], ['ぷ', 'pu'], ['ぺ', 'pe'], ['ぽ', 'po']]
}

const katakana = {
    ø: [['ア', 'a'], ['イ', 'i'], ['ウ', 'u'], ['エ', 'e'], ['オ', 'o']],
    k: [['カ', 'ka'], ['キ', 'ki'], ['ク', 'ku'], ['ケ', 'ke'], ['コ', 'ko']],
    s: [['サ', 'sa'], ['シ', 'shi'], ['ス', 'su'], ['セ', 'se'], ['ソ', 'so']],
    t: [['タ', 'ta'], ['チ', 'chi'], ['ツ', 'tsu'], ['テ', 'te'], ['ト', 'to']],
    n: [['ナ', 'na'], ['ニ', 'ni'], ['ヌ', 'nu'], ['ネ', 'ne'], ['ノ', 'no']],
    h: [['ハ', 'ha'], ['ヒ', 'hi'], ['フ', 'fu'], ['ヘ', 'he'], ['ホ', 'ho']],
    m: [['マ', 'ma'], ['ミ', 'mi'], ['ム', 'mu'], ['メ', 'me'], ['モ', 'mo']],
    y: [['ヤ', 'ya'], ['', ''], ['ユ', 'yu'], ['', ''], ['ヨ', 'yo']],
    r: [['ラ', 'ra'], ['リ', 'ri'], ['ル', 'ru'], ['レ', 're'], ['ロ', 'ro']],
    w: [['ワ', 'wa'], ['', ''], ['ン', 'n'], ['', ''], ['ヲ', 'wo']],
    g: [['ガ', 'ga'], ['ギ', 'gi'], ['グ', 'gu'], ['ゲ', 'ge'], ['ゴ', 'go']],
    z: [['ザ', 'za'], ['ジ', 'ji'], ['ズ', 'zu'], ['ゼ', 'ze'], ['ゾ', 'zo']],
    d: [['ダ', 'da'], ['ヂ', 'di'], ['ヅ', 'du'], ['デ', 'de'], ['ド', 'do']],
    b: [['バ', 'ba'], ['ビ', 'bi'], ['ブ', 'bu'], ['ベ', 'be'], ['ボ', 'bo']],
    p: [['パ', 'pa'], ['ピ', 'pi'], ['プ', 'pu'], ['ペ', 'pe'], ['ポ', 'po']]
}

//
// VARIABLES
//

let cards = []
// let ans = '   '
let game
let charArr = []
let indexArr = []
let board
let inputDiv
let table
let option
let info
let timeSpan
let foundSpan
let indexArrLengthSpan
let missedSpan
let hiraCB
let kataCB
let nbCartesInput
let kanaOccurences
let autoAddCardDelay
let tableVisible
let thVisible
// let commandes
let outputDiv
let chooseCharsDiv
let autoReplay

//
// JEU
//

document.addEventListener('DOMContentLoaded', function init () {
    // Code exécuté au chargement de la page
    document.removeEventListener('DOMContentLoaded', init)

    // Générer la page :

    build()

    // Commencer la partie :

    optionInput({ target: { id: 'hira_cb' } })

    document.addEventListener('keydown', keyPressed)
    document.addEventListener('click', function (event) {
        inputDiv.focus({ preventScroll: true })
    })
})

function build () {
    // Créer le panneau d'options :

    option = document.createElement('div')
    option.id = 'option'

    let div = document.createElement('div')

    let div2 = document.createElement('div')

    hiraCB = document.createElement('input')
    hiraCB.id = 'hira_cb'
    hiraCB.type = 'checkbox'
    hiraCB.checked = true
    div2.appendChild(hiraCB)

    let label = document.createElement('label')
    label.htmlFor = 'hira_cb'
    label.textContent = 'hiragana'
    div2.appendChild(label)

    div.appendChild(div2)
    div2 = document.createElement('div')

    kataCB = document.createElement('input')
    kataCB.id = 'kata_cb'
    kataCB.type = 'checkbox'
    div2.appendChild(kataCB)

    label = document.createElement('label')
    label.htmlFor = 'kata_cb'
    label.textContent = 'katakana'
    div2.appendChild(label)

    div.appendChild(div2)

    let button = document.createElement('button')
    button.id = 'kanaChoice'
    button.textContent = 'plus de choix'
    div.appendChild(button)

    option.appendChild(div)

    div = document.createElement('div')
    div.className = 'mobile-hidden'

    div2 = document.createElement('div')

    nbCartesInput = document.createElement('input')
    nbCartesInput.id = 'nb_cartes'
    nbCartesInput.type = 'number'
    nbCartesInput.value = '1'
    nbCartesInput.min = '1'
    nbCartesInput.max = '10'
    div2.appendChild(nbCartesInput)

    label = document.createElement('label')
    label.htmlFor = 'nb_cartes'
    label.textContent = 'nombre de cartes en jeu'
    div2.appendChild(label)

    div.appendChild(div2)
    div2 = document.createElement('div')

    kanaOccurences = document.createElement('input')
    kanaOccurences.id = 'kana_occurences'
    kanaOccurences.type = 'number'
    kanaOccurences.value = '1'
    kanaOccurences.min = '1'
    kanaOccurences.max = '9'
    div2.appendChild(kanaOccurences)

    label = document.createElement('label')
    label.htmlFor = 'kana_occurences'
    label.textContent = 'occurence des caractères'
    div2.appendChild(label)

    div.appendChild(div2)
    div2 = document.createElement('div')

    autoAddCardDelay = document.createElement('input')
    autoAddCardDelay.id = 'new_card_delay'
    autoAddCardDelay.type = 'number'
    autoAddCardDelay.step = '0.1'
    autoAddCardDelay.value = '3'
    autoAddCardDelay.min = '0'
    autoAddCardDelay.max = '10'
    div2.appendChild(autoAddCardDelay)

    label = document.createElement('label')
    label.htmlFor = 'new_card_delay'
    label.textContent = 'délai d\'apparition des cartes'
    div2.appendChild(label)

    div.appendChild(div2)

    option.appendChild(div)

    div = document.createElement('div')
    div.className = 'mobile-hidden'

    div2 = document.createElement('div')

    tableVisible = document.createElement('input')
    tableVisible.id = 'table_visible'
    tableVisible.type = 'checkbox'
    div2.appendChild(tableVisible)

    label = document.createElement('label')
    label.htmlFor = 'table_visible'
    label.textContent = 'afficher le tableau de caractères'
    div2.appendChild(label)

    div.appendChild(div2)
    div2 = document.createElement('div')

    thVisible = document.createElement('input')
    thVisible.id = 'th_visible'
    thVisible.type = 'checkbox'
    div2.appendChild(thVisible)

    label = document.createElement('label')
    label.htmlFor = 'th_visible'
    label.textContent = 'afficher les romaji dans le tableau'
    div2.appendChild(label)

    div.appendChild(div2)

    option.appendChild(div)

    div = document.createElement('div')
    div2 = document.createElement('div')
    autoReplay = document.createElement('input')
    autoReplay.id = 'auto_replay'
    autoReplay.type = 'checkbox'
    div2.appendChild(autoReplay)
    label = document.createElement('label')
    label.htmlFor = 'auto_replay'
    label.textContent = 'autoreplay'
    div2.appendChild(label)
    div.appendChild(div2)
    button = document.createElement('button')
    button.id = 'replay'
    button.textContent = 'rejouer'
    div.appendChild(button)
    option.appendChild(div)

    option.addEventListener('input', optionInput)
    option.addEventListener('click', buttonClicked)
    document.body.appendChild(option)

    // Créer le div#game :

    game = document.createElement('div')
    game.id = 'game'

    board = document.createElement('div')
    board.id = 'board'
    board.addEventListener('dblclick', reveal)
    game.appendChild(board)

    inputDiv = document.createElement('textarea')
    inputDiv.id = 'input'
    inputDiv.spellcheck = false
    inputDiv.value = '   '
    game.appendChild(inputDiv)

    table = document.createElement('table')
    table.className = 'mobile-hidden'
    table.style.display = (event.target.checked ? '' : 'none')
    table.addEventListener('click', reveal)
    game.appendChild(table)

    document.body.appendChild(game)

    // Créer le div#commandes :

    /* commandes = document.createElement('div')
    commandes.id = 'commandes'

    div = document.createElement('div')
    div.textContent = 'a-z : entrer du texte'
    commandes.appendChild(div)

    div = document.createElement('div')
    div.textContent = '↵ : valider l\'entrée' // ↩↲
    commandes.appendChild(div)

    div = document.createElement('div')
    div.textContent = '⇤ : effacer le dernier caractère' // ←⇽
    commandes.appendChild(div)

    div = document.createElement('div')
    div.textContent = 'Esc : effacer toute l\'entrée'
    commandes.appendChild(div)

    div = document.createElement('div')
    div.textContent = 'double-clic sur carte : afficher la traduction'
    commandes.appendChild(div)

    div = document.createElement('div')
    div.textContent = 'clic dans tableau : afficher la traduction'
    commandes.appendChild(div)

    document.body.appendChild(commandes) */

    // Créer le div#info :

    info = document.createElement('div')
    info.id = 'info'

    div = document.createElement('div')

    let span = document.createElement('span')
    span.textContent = 'Temps : '
    div.appendChild(span)

    timeSpan = document.createElement('span')
    div.appendChild(timeSpan)

    info.appendChild(div)

    div = document.createElement('div')

    span = document.createElement('span')
    span.textContent = 'Trouvés : '
    div.appendChild(span)

    foundSpan = document.createElement('span')
    div.appendChild(foundSpan)

    span = document.createElement('span')
    span.textContent = ' / '
    div.appendChild(span)

    indexArrLengthSpan = document.createElement('span')
    div.appendChild(indexArrLengthSpan)

    info.appendChild(div)

    div = document.createElement('div')

    span = document.createElement('span')
    span.textContent = 'Manqués : '
    div.appendChild(span)

    missedSpan = document.createElement('span')
    div.appendChild(missedSpan)

    info.appendChild(div)

    document.body.appendChild(info)

    // Créer le div#output :

    outputDiv = document.createElement('div')
    outputDiv.id = 'output'
    document.body.appendChild(outputDiv)

    // Créer le div#chooseChars :

    chooseCharsDiv = document.createElement('div')
    chooseCharsDiv.id = 'chooseChars'
    chooseCharsDiv.style.display = 'none'
    chooseCharsDiv.addEventListener('click', chooseChar)
    chooseCharsDiv.addEventListener('mouseover', charHover)
    chooseCharsDiv.addEventListener('mouseout', charLeave)
    div = document.createElement('table')
    fillCharTable(div, hiragana)
    chooseCharsDiv.appendChild(div)
    div = document.createElement('table')
    fillCharTable(div, katakana)
    chooseCharsDiv.appendChild(div)
    button = document.createElement('button')
    button.textContent = 'valider'
    chooseCharsDiv.appendChild(button)
    document.body.appendChild(chooseCharsDiv)
}

function addCard () {
    const card = document.createElement('div')
    card.className = 'card'
    card.char = charArr[indexArr.pop()]
    // card.char = charArr[indexArr.splice(getRandomIndex(indexArr.length), 1)[0]] // pour plus de randomness
    // card.char = spliceRandomValue(charArr) // pour plus de randomness plus simplement
    card.textContent = card.char[0]
    cards.push(card)
    board.appendChild(card)
}

let autoAddCardID
function autoAddCard () {
    addCard()
    autoAddCardID = setInterval(function () {
        if (indexArr.length < 1) {
            clearTimeout(autoAddCardID)
        } else if (cards.length < nbCartesInput.value) {
            addCard()
        }
    }, autoAddCardDelay.value * 1000)
}

let timerID
function timer () {
    ++timeSpan.textContent
    timerID = setTimeout(timer, 1000)
}

function fillCharTable (htmlTable, ...args) {
    htmlTable.innerHTML = ''
    for (const kanaArr of args) {
        /*
        let tr = document.createElement('tr')
        let td = document.createElement('th')
        td.colSpan = kanaArr[consonants[0]].length + 1
        console.log(args.keys().next)
        td.textContent = 'nom du tableau'
        tr.appendChild(td)
        htmlTable.appendChild(tr)
        */
        let tr = document.createElement('tr')
        let td = document.createElement('th')
        tr.appendChild(td)
        for (const char of kanaArr[consonants[0]]) {
            td = document.createElement('th')
            td.textContent = char[1]
            tr.appendChild(td)
        }
        htmlTable.appendChild(tr)
        for (const consonant of consonants) {
            tr = document.createElement('tr')
            td = document.createElement('th')
            td.textContent = consonant
            tr.appendChild(td)
            for (const char of kanaArr[consonant]) {
                td = document.createElement('td')
                td.char = char
                td.textContent = td.char[0]
                tr.appendChild(td)
            }
            htmlTable.appendChild(tr)
        }
    }
}

function fillCharTable2 (htmlTable, ...htmlTables) {
    htmlTable.innerHTML = ''
    for (const table of htmlTables) {
        let ths = new Set()
        const tds = new Map()
        for (let y = 1; y < table.children.length; y++) {
            const arr = []
            for (let x = 1; x < table.children[y].children.length; x++) {
                if (table.children[y].children[x].style.textDecoration === 'underline') {
                    ths.add(x)
                    arr.push(x)
                }
            }
            tds.set(y, arr)
        }

        if (ths.size > 0) {
            const arr = []
            ths.forEach(element => {
                arr.push(element)
            })
            ths = arr.sort(function () { return arguments[0] - arguments[1] })

            let tr = document.createElement('tr')
            let td = document.createElement('th')
            td.textContent = table.children[0].children[0].textContent
            tr.appendChild(td)
            for (const x of ths) {
                td = document.createElement('th')
                td.textContent = table.children[0].children[x].textContent
                tr.appendChild(td)
            }
            htmlTable.appendChild(tr)

            for (const y of tds.keys()) {
                tr = document.createElement('tr')
                if (tds.get(y).length > 0) {
                    td = document.createElement('th')
                    td.textContent = table.children[y].children[0].textContent
                    tr.appendChild(td)
                    for (const x of ths) {
                        td = document.createElement('td')
                        if (x === tds.get(y)[0]) {
                            td.char = table.children[y].children[tds.get(y).shift()].char // !
                            td.textContent = td.char[0]
                        }
                        tr.appendChild(td)
                    }
                }
                htmlTable.appendChild(tr)
            }
        }
    }
}

function checkAnswer () {
    const cardsLength = cards.length // car autoAddCard peut incrémenter ce nombre pendant l'execution
    let match = inputDiv.value.trim() === ''
    let i = -1
    while (!match && ++i < cardsLength) {
        if (inputDiv.value.trim().toLowerCase() === cards[i].char[1]) {
            ++foundSpan.textContent
            fadeFoundChar(cards[i].char[0])
            cards.splice(i, 1)[0].remove()
            if (cards.length < 1) {
                if (indexArr.length < 1) {
                // Fin de partie
                    if (autoReplay.checked) {
                        optionInput({ target: { id: 'kana_occurences' } })
                    } else {
                        clearTimeout(timerID)
                        outputDiv.textContent = 'Bravo ! Vous avez trouvé le' + (foundSpan.textContent < 2 ? '' : 's ' + foundSpan.textContent) +
                        ' caractère' + (foundSpan.textContent < 2 ? '' : 's') + ' en ' +
                        (timeSpan.textContent / 60 < 1 ? '' : Math.floor(timeSpan.textContent / 60) + ' minute' + (timeSpan.textContent / 60 < 2 ? '' : 's')) +
                        ((!(timeSpan.textContent / 60 < 1) && !(timeSpan.textContent % 60 < 1)) ? ' et ' : ' ') +
                        (timeSpan.textContent % 60 < 1 ? '' : timeSpan.textContent % 60 + ' seconde' + (timeSpan.textContent % 60 < 2 ? '' : 's')) +
                        (missedSpan.textContent < 1 ? '' : ', avec ' + missedSpan.textContent + ' erreur' + (missedSpan.textContent < 2 ? '' : 's')) +
                        ' !'
                    }
                } else {
                    clearTimeout(autoAddCardID)
                    autoAddCard()
                }
            }
            match = true
        }
    }
    if (i === cardsLength) {
        ++missedSpan.textContent
    }
}

function fadeFoundChar (char) {
    for (const td of table.getElementsByTagName('td')) {
        if (td.char && td.char[0] === char) {
            const newRGBValue = getRGBValue(td.style.color) - 255 / kanaOccurences.value
            td.style.color = 'rgb(' + newRGBValue + ',' + newRGBValue + ',' + newRGBValue + ')'
            break
        }
    }
}

function getRGBValue (styleColor) {
    let value
    if (styleColor === '') {
        value = 255
    } else {
        value = Number.parseInt(styleColor.split(',')[1])
    }
    return value
}

function resetFoundChar () {
    for (const td of table.getElementsByTagName('td')) {
        td.style.color = 'rgb(' + 255 + ',' + 255 + ',' + 255 + ')'
    }
}

//
// EVENTS
//

function keyPressed (event) {
    event.preventDefault()
    if (event.keyCode > 64 && event.keyCode < 91) {
        // On tape une lettre de l'alphabet
        inputDiv.value = inputDiv.value.substring(1) + event.key.toLowerCase()
    } else if (event.keyCode === 13) {
        // On tape sur 'Enter'
        checkAnswer()
        // inputDiv.value = ' '.repeat(inputDiv.value.length)
        inputDiv.value = '   '
    } else if (event.keyCode === 8) {
        // On tape sur 'Backspace'
        inputDiv.value = ' ' + inputDiv.value.substring(0, inputDiv.value.length - 1)
    } else if (event.keyCode === 27) {
        // On tape sur 'Echap'
        inputDiv.value = ' '.repeat(inputDiv.value.length)
    }
}

function optionInput (event) {
    switch (event.target.id) {
    case 'hira_cb':
    case 'kata_cb':
        wipechooseCharsDiv()
        if (hiraCB.checked && !kataCB.checked) {
            charArr = getKanaRomajiArray(hiragana)
            fillCharTable(table, hiragana)
            chooseCharsDiv.children[0].children[0].children[0].click()
        } else if (!hiraCB.checked && kataCB.checked) {
            charArr = getKanaRomajiArray(katakana)
            fillCharTable(table, katakana)
            chooseCharsDiv.children[1].children[0].children[0].click()
        } else if (hiraCB.checked && kataCB.checked) {
            charArr = getKanaRomajiArray(hiragana, katakana)
            fillCharTable(table, hiragana, katakana)
            chooseCharsDiv.children[0].children[0].children[0].click()
            chooseCharsDiv.children[1].children[0].children[0].click()
        } else if (!hiraCB.checked && !kataCB.checked) {
            charArr = getKanaRomajiArray()
            fillCharTable(table)
        }
        /* eslint-enable no-fallthrough */
    case 'kana_occurences':
    case 'nb_cartes':
    case 'new_card_delay':
        indexArr = fillArrayWithRandomFixedOccurences(charArr.length, kanaOccurences.value)
        clearTimeout(autoAddCardID)
        clearTimeout(timerID)
        board.innerHTML = ''
        cards = []
        foundSpan.textContent = 0
        indexArrLengthSpan.textContent = indexArr.length
        missedSpan.textContent = 0
        timeSpan.textContent = 0
        outputDiv.textContent = ''
        if (indexArr.length > 0) {
            autoAddCard()
            timer()
        }
        /* eslint-enable no-fallthrough */
    case 'th_visible':
        const value = (thVisible.checked ? '' : 'none')
        for (const th of table.getElementsByTagName('th')) {
            th.style.display = value
        }
        break
    case 'table_visible':
        table.style.display = (event.target.checked ? '' : 'none')
        break
    }
}

function reveal (event) {
    if (event.target.tagName === 'TD' || event.target.className === 'card') {
        event.target.textContent = event.target.textContent === event.target.char[0] ? event.target.char[1] : event.target.char[0]
    }
}

function buttonClicked (event) {
    switch (event.target.id) {
    case 'replay':
        optionInput({ target: { id: 'kana_occurences' } })
        resetFoundChar()
        event.target.blur()
        break
    case 'kanaChoice':
        option.style.display = 'none'
        game.style.display = 'none'
        info.style.display = 'none'
        outputDiv.style.display = 'none'
        chooseCharsDiv.style.display = 'grid'
        break
    }
}

function chooseChar (event) {
    switch (event.target.nodeName) {
    case 'TD':
        if (event.target.char[0] !== '') {
            event.target.style.textDecoration = event.target.style.textDecoration ? '' : 'underline'
        }
        break
    case 'TH':
        if (event.target.parentElement.previousElementSibling === null) {
            if (event.target.previousElementSibling === null) {
                for (const tr of event.target.parentElement.parentElement.children) {
                    for (const td of tr.children) {
                        if (td.nodeName === 'TD' && td.char[0] !== '') {
                            td.style.textDecoration = td.style.textDecoration ? '' : 'underline'
                        }
                    }
                }
            } else {
                let i = 0
                while (event.target.parentElement.children[++i] !== event.target) {}
                for (const tr of event.target.parentElement.parentElement.children) {
                    if (tr.children[i].nodeName === 'TD' && tr.children[i].char[0] !== '') {
                        tr.children[i].style.textDecoration = tr.children[i].style.textDecoration ? '' : 'underline'
                    }
                }
            }
        } else {
            for (const td of event.target.parentElement.children) {
                if (td.nodeName === 'TD' && td.char[0] !== '') {
                    td.style.textDecoration = td.style.textDecoration ? '' : 'underline'
                }
            }
        }
        break
    case 'BUTTON':
        hiraCB.checked = false
        kataCB.checked = false
        charArr = getKanaRomajiArray2()
        // fillCharTable(table, hiragana, katakana)
        fillCharTable2(table, chooseCharsDiv.children[0], chooseCharsDiv.children[1])
        optionInput({ target: { id: 'kana_occurences' } })
        chooseCharsDiv.style.display = 'none'
        option.style.display = 'flex'
        game.style.display = 'grid'
        info.style.display = 'flex'
        outputDiv.style.display = ''
        break
    }
}

function charHover (event, color = 'red') {
    switch (event.target.nodeName) {
    case 'TD':
        event.target.style.color = color
        break
    case 'TH':
        if (event.target.parentElement.previousElementSibling === null) {
            if (event.target.previousElementSibling === null) {
                event.target.parentElement.parentElement.style.color = color
            } else {
                let i = 0
                while (event.target.parentElement.children[++i] !== event.target) {}
                for (const tr of event.target.parentElement.parentElement.children) {
                    tr.children[i].style.color = color
                }
            }
        } else {
            event.target.parentElement.style.color = color
        }
        break
    }
}

function charLeave (event) {
    charHover(event, '')
}

//
// UTILITAIRES
//

/*
function getRandomIndex (arrayLength) {
    // Retourne un index de tableau aléatoire
    return Math.floor(Math.random() * (arrayLength))
}

function getRandomValue (array) {
    // Retourne la valeur a un index aléatoire de tableau
    return array[Math.floor(Math.random() * (array.length))]
}

function spliceRandomValue (array) {
    // Supprime et retourne la valeur a un index aléatoire de tableau
    return array.splice(Math.floor(Math.random() * (array.length)), 1)
}
*/

function fillArrayWithRandomFixedOccurences (lim, nb) {
    // Retourne un tableau de longueur lim*nb avec des nombres entiers aléatoires compris entre 0 et lim, tous présents nb fois
    const array = new Array(lim * nb)
    let val
    for (let i = 0; i < lim * nb; i++) {
        do {
            val = Math.floor(Math.random() * (lim))
        } while (array.filter(function (value) {
            return value === val
        }).length > nb - 1)
        array[i] = val
    }
    return array
}

function getKanaRomajiArray () {
    const array = []
    for (const kanaArr of arguments) {
        for (const consonant of consonants) {
            for (const charPair of kanaArr[consonant]) {
                if (charPair[0] !== '') {
                    array.push(charPair)
                }
            }
        }
    }
    return array
}

function getKanaRomajiArray2 () {
    const array = []
    for (const table of chooseCharsDiv.children) {
        for (const tr of table.children) {
            for (const td of tr.children) {
                if (td.nodeName === 'TD' && td.style.textDecoration) {
                    array.push(td.char)
                }
            }
        }
    }
    return array
}

function wipechooseCharsDiv () {
    for (const table of chooseCharsDiv.children) {
        for (const tr of table.children) {
            for (const td of tr.children) {
                td.style.textDecoration = ''
            }
        }
    }
}
